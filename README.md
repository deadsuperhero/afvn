# AGS Framework for Visual Novels

Welcome to the **AGS Framework for Visual Novels**, or **AFVN** for short!
AFVN is designed to take the default functionality of
[Adventure Game Studio](https://www.adventuregamestudio.co.uk/) and
extend it to resemble games such as Phoenix Wright: Ace Attorney,
Steins;Gate, or even Doki Doki Literature Club.

This repo is currently a fledgling side-project with only a little bit
of code and not many assets. I'm currently putting together the core
of the script module, and building a demo game around it to demonstrate
the custom functionality.

The goal is to produce a simple project with some well-documented code
to ensure that it's easy to use for anybody that's interested in doing
something a little bit unique with my favorite game authoring tool.

## Documentation
Check out this repo's [wiki](https://codeberg.org/deadsuperhero/afvn/wiki) for information on custom functions and
demonstrations of how they are used. Currently WIP.

## Demo Credits
* AFVN script and UI by Sean Tilley (@deadsuperhero)
* Background song is ["Energetic Uplifting Pop"](https://pixabay.com/music/upbeat-energetic-uplifting-pop-110058/)
by [Music for Videos](https://pixabay.com/users/music_for_videos-26992513/) on Pixabay.
