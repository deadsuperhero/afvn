// new module header
enum expression {
// These are the emotions that every character in-game is capable of expressing.
// We use this to set the expression as a parameter in a custom Say function.
  Happy, 
  Sad, 
  Angry, 
  Surprised, 
  Confused
};

enum charPos {
// Used to express character positions on-screen. Useful for telling a
// character when to position themselves to a side of the screen
  Left, 
  Center, 
  Right
};

// Some useful variables to expose

import String afvn_version;
import String declared_expression;
import int selection;
import int highlight;
import GUI *activeGUI;
import bool messageRead;

// Custom functions to expose to scripts

import function getVersion();
import function isPresent(this Character*);
import function joinMe(this Character*);
import function leaveMe(this Character*);
import function Desc(String description);
import function Speak(this Character*, expression param, String body);
import function shiftPos(this Character*, charPos param);
import function hoverLabel();
import function openGUI(GUI *toOpen);
import function closeGUI(GUI *toClose);
